class CellsBfmIvControllerRegistration extends Polymer.Element {

  static get is() {
    return 'cells-bfm-iv-controller-registration';
  }

  static get properties() {
    return {
      dataForm: {
        type: Object,
        value: () => ({})
      }
    };
  }

  setDataCredentialsWrraper(){
    let formConfig = {
      'title' : {
        'text': 'Ingrese credenciales SAT'
      },
      'hasError':false,
      'form' : {
        'text': 'Elija el RFC que desee agregar para consultar las facturas, y enseguida ingrese la clave CIEC correspondiente a la razón social.',
        'errorPassword': 'El usuario o contraseña ingresados son incorrectos. Verifique y vuelva a intentarlo.<br>En caso de no recordarlas acuda a su banco para solicitar sus accesos.',
        'errorAttemp': 'Unicamente cuenta con 3 intentos para accesar sin que su banca sea bloqueada',
        'buttonDisabled': true,
        'buttonText': 'Siguiente'
      },
      'inputPassword' : {
        'name': 'ciec',
        'icon': 'coronita:visualize',
        'toggleIcon': 'coronita:hide',
        'placeholder': 'Ingrese su clave CIEC'
      },
      'inputAccount' : {
        'name': 'rfc',
        'placeholder': 'RFC asociado al SAT',
        'accounts': [{value: 'ACM010101ABC', label: 'ACME CORP SA DE CV'}]
      }
    };
    this.dataForm = formConfig;
    let helperPrimaryConfig = {
      'type':'info',
      'class':'primary',
      'icon' :'coronita:security',
      'title': 'Seguridad BBVA',
      'message':'En BBVA conocemos la necesidad de garantizar el flujo seguro de información entre el banco y sus clientes.</br><strong style=\'margin-top:1rem;display:block;\'>Por ello le garantizamos:</strong>',
      'list':[
        'Confidencialidad en las comunicaciones entre sus bancos y usted. ',
        'Visión segura de sus finanzas empresariales.',
        'Certeza de que no haremos uso indebido de sus datos.'
      ]
    };
    let helperSecondaryConfig = {
      'class':'secondary',
      'type':'error',
      'title': '¿Necesita ayuda?',
      'message':'Comuníquese con nosotros de lunes a viernes de <b>7 a 20 h</b> y los sábados de <b>9 a 18 h</b> al número:</br><b>55 1998 8080 </b>'
    };
    this.dispatchEvent(new CustomEvent('helper-secondary-config', {detail: helperSecondaryConfig}))
    this.dispatchEvent(new CustomEvent('helper-primary-config', {detail: helperPrimaryConfig}))
    this.dispatchEvent(new CustomEvent('form-config', {detail: this.dataForm}))
  }

  hasErrorCredentials(){
    this.set('dataForm.hasError', true);
    this.dataForm = JSON.parse(JSON.stringify(this.dataForm));
    this.dispatchEvent(new CustomEvent('form-config', {detail:this.dataForm}));
  }
}

customElements.define(CellsBfmIvControllerRegistration.is, CellsBfmIvControllerRegistration);
